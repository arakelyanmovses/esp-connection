const controllerText = document.getElementById('controller_text')
const copleteText = document.getElementById('complete_text')
const switchView = document.getElementById('switch')

firebase.database().ref("LED_Status").set(-1);
firebase.database().ref("LED_Response").set(-1);

controllerText.style.color = "#2196F3";
controllerText.textContent = "Turn ON remote led lamp"

firebase.database().ref("LED_Response").on('value', function(snapshot) {
    switch (snapshot.val()) {
        case 0:
            copleteText.textContent = "The process of turning ON the LED lamp is failed!!";
            copleteText.style.color = "#cf250e";

            switchView.checked = false

            controllerText.style.color = "#2196F3";
            controllerText.textContent = "Turn ON remote led lamp"
            break;

        case 1:
            copleteText.textContent = "The process of turning on the LED lamp is completed successfully";
            copleteText.style.color = "#2196F3";
            break;
        default:
            copleteText.textContent = "";
            break;
    }
});

switchView.addEventListener('change', function(e) {
    const isChecked = e.target.checked
    if (isChecked) {
        makeRequest(1)

        controllerText.style.color = "#cf250e";
        controllerText.textContent = "Turn OFF remote led lamp"
    } else {
        makeRequest(0)

        controllerText.style.color = "#2196F3";
        controllerText.textContent = "Turn ON remote led lamp"
    }
});

function makeRequest(identifire) {
    firebase.database().ref("LED_Status").set(identifire)

    firebase.database().ref("LED_Response").set(-1);
}

firebase.database().ref().on('value', function(snapshot) {
    document.getElementById('db_data_snapshot_stat').textContent = JSON.stringify(snapshot.val())
})